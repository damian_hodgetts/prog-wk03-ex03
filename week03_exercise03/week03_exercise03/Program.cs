﻿using System;

namespace week03_exercise03
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var colours = new string[5] { "red", "blue", "orange", "white", "black" };
			string.Join(",", colours);
			Console.WriteLine(string.Join(",",colours));
		}
	}
}
